FROM alpine:3.18

LABEL maintainer="Tom Winterhalder <tom.winterhalder@gmail.com>"

RUN apk add --no-cache tzdata=2023c-r1 curl=8.4.0-r0 openssl=3.1.4-r1 \
    ca-certificates=20230506-r0 && \
    update-ca-certificates && \
    cp /usr/share/zoneinfo/Europe/Zurich /etc/localtime && \
    echo "Europe/Zurich" > /etc/timezone && \
    addgroup -g 1000 basegroup && \
    adduser -u 1000 -G basegroup -D baseuser && \
    rm -f /var/cache/dpk/*
