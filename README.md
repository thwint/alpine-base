# alpine-base #
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/thwint/alpine-base/master)
![Docker Image Size (latest by date)](https://img.shields.io/docker/image-size/thwint/alpine-base)
![Docker Image Version (latest by date)](https://img.shields.io/docker/v/thwint/alpine-base)
![License MIT](https://img.shields.io/badge/license-MIT-blue.svg)

Custom docker base image based on Alpine linux. It is mainly for personal use only. Feel free to use and contribute to it.

## Image details ##

* Timezone set to Europe/Zurich
* Unprivileged user and group (baseuser:basegroup) both with id 1000

### Packages ###
* ca-certrificates
* curl
* openssl
* tzdata

